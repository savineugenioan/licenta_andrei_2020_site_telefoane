-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 21, 2021 at 12:04 AM
-- Server version: 8.0.19
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shopee`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
CREATE TABLE IF NOT EXISTS `cart` (
  `cart_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `item_id` int NOT NULL,
  PRIMARY KEY (`cart_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comenzi`
--

DROP TABLE IF EXISTS `comenzi`;
CREATE TABLE IF NOT EXISTS `comenzi` (
  `Id_Comanda` int NOT NULL AUTO_INCREMENT,
  `c_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Id_User` int DEFAULT NULL,
  `status_comanda` varchar(10) DEFAULT 'NEACHITAT',
  PRIMARY KEY (`Id_Comanda`),
  KEY `Id_User` (`Id_User`)
) ;

--
-- Dumping data for table `comenzi`
--

INSERT INTO `comenzi` (`Id_Comanda`, `c_date`, `Id_User`, `status_comanda`) VALUES
(6, '2021-06-20 16:35:52', 1, 'NEACHITAT'),
(7, '2021-06-20 16:37:56', 1, 'NEACHITAT'),
(8, '2021-06-20 16:38:49', 1, 'NEACHITAT'),
(9, '2021-06-20 18:05:11', 1, 'NEACHITAT'),
(10, '2021-06-20 18:07:15', 1, 'NEACHITAT'),
(21, '2021-06-20 22:52:47', 3, 'NEACHITAT'),
(22, '2021-06-20 22:53:08', 1, 'NEACHITAT'),
(23, '2021-06-21 00:00:17', 1, 'NEACHITAT'),
(24, '2021-06-21 00:00:36', 3, 'NEACHITAT');

-- --------------------------------------------------------

--
-- Table structure for table `comenzi_detalii`
--

DROP TABLE IF EXISTS `comenzi_detalii`;
CREATE TABLE IF NOT EXISTS `comenzi_detalii` (
  `Id_c_detalii` int NOT NULL AUTO_INCREMENT,
  `Produs` varchar(30) NOT NULL,
  `Cantitate` int NOT NULL,
  `Pret_Unitar` int NOT NULL,
  `Id_comanda` int DEFAULT NULL,
  PRIMARY KEY (`Id_c_detalii`),
  KEY `Id_comanda` (`Id_comanda`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `comenzi_detalii`
--

INSERT INTO `comenzi_detalii` (`Id_c_detalii`, `Produs`, `Cantitate`, `Pret_Unitar`, `Id_comanda`) VALUES
(1, '4', 2, 122, 6),
(2, '12', 1, 152, 6),
(3, '4', 1, 122, 7),
(4, '12', 1, 152, 7),
(5, '4', 1, 122, 8),
(6, '12', 3, 152, 8),
(7, '9', 1, 152, 9),
(8, '7', 1, 122, 10),
(12, '12', 3, 152, 21),
(13, '10', 1, 152, 22),
(14, '3', 4, 122, 22),
(15, '12', 1, 152, 23),
(16, '9', 1, 152, 24);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `item_id` int NOT NULL AUTO_INCREMENT,
  `item_brand` varchar(200) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_price` double(10,2) NOT NULL,
  `item_image` varchar(255) NOT NULL,
  `item_register` datetime DEFAULT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`item_id`, `item_brand`, `item_name`, `item_price`, `item_image`, `item_register`) VALUES
(1, 'Samsung', 'Samsung Galaxy 10', 152.00, 'assets/products/1.png', '2020-03-28 11:08:57'),
(2, 'Redmi', 'Redmi Note 7', 122.00, './assets/products/2.png', '2020-03-28 11:08:57'),
(3, 'Redmi', 'Redmi Note 6', 122.00, './assets/products/3.png', '2020-03-28 11:08:57'),
(4, 'Redmi', 'Redmi Note 5', 122.00, './assets/products/4.png', '2020-03-28 11:08:57'),
(5, 'Redmi', 'Redmi Note 4', 122.00, './assets/products/5.png', '2020-03-28 11:08:57'),
(6, 'Redmi', 'Redmi Note 8', 122.00, './assets/products/6.png', '2020-03-28 11:08:57'),
(7, 'Redmi', 'Redmi Note 9', 122.00, './assets/products/7.png', '2020-03-28 11:08:57'),
(8, 'Redmi', 'Redmi Note', 122.00, './assets/products/8.png', '2020-03-28 11:08:57'),
(9, 'Samsung', 'Samsung Galaxy S6', 152.00, './assets/products/9.png', '2020-03-28 11:08:57'),
(10, 'Samsung', 'Samsung Galaxy S7', 152.00, './assets/products/10.png', '2020-03-28 11:08:57'),
(11, 'Apple', 'Apple iPhone 5', 152.00, './assets/products/11.png', '2020-03-28 11:08:57'),
(12, 'Apple', 'Apple iPhone 6', 152.00, './assets/products/12.png', '2020-03-28 11:08:57');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `Id_User` int NOT NULL AUTO_INCREMENT,
  `is_admin` int DEFAULT '0',
  `username` varchar(14) NOT NULL,
  `password` varchar(30) NOT NULL,
  `email` varchar(40) NOT NULL,
  `adress` varchar(70) NOT NULL DEFAULT '-',
  PRIMARY KEY (`Id_User`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`Id_User`, `is_admin`, `username`, `password`, `email`, `adress`) VALUES
(1, 1, 'afilip', '123456', 'afilip@yahoo.com', '123444'),
(3, 0, 'maria', '123456', 'asd@yahoo.com', '12344');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

DROP TABLE IF EXISTS `wishlist`;
CREATE TABLE IF NOT EXISTS `wishlist` (
  `cart_id` int NOT NULL,
  `user_id` int NOT NULL,
  `item_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comenzi`
--
ALTER TABLE `comenzi`
  ADD CONSTRAINT `comenzi_ibfk_1` FOREIGN KEY (`Id_User`) REFERENCES `users` (`Id_User`);

--
-- Constraints for table `comenzi_detalii`
--
ALTER TABLE `comenzi_detalii`
  ADD CONSTRAINT `comenzi_detalii_ibfk_1` FOREIGN KEY (`Id_comanda`) REFERENCES `comenzi` (`Id_Comanda`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
