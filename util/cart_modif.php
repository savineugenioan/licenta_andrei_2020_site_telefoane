<?php
session_start();
$op = $_POST['op'];
$produs = $_POST['produs'];
$id = $_POST['id'];

if(!isset($_SESSION['cart'][$id])){
    $_SESSION['cart'][$id]['count']=0;
    $_SESSION['cart'][$id]['produs']=$produs;
}
    
if($op =="+"){
    $_SESSION['cart'][$id]['count']+=1;
}
elseif($op =='-' && $_SESSION['cart'][$id]['count']>0){
    $_SESSION['cart'][$id]['count']-=1;
}
if($_SESSION['cart'][$id]['count'] ==0){
    unset($_SESSION['cart'][$id]);
}
?>