<!DOCTYPE html>
<html lang="en">
<head>
  <?php
  include("header.php");
  $def_source = "dragon.jpg";
  ?>
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
  <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">
      <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
      <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
  <link rel="stylesheet" type="text/css" href="css/comenzi.css">
</head>
<style>

.iframe_pdf {
    position: relative !important;
    top: 0  !important; left: 0  !important;
    width:100%  !important;
    height: 100vh !important;
}
.ui-dialog{
    width:90%  !important;
    height: 100vh !important;
    
}
</style>
<?php
$id_user=$_SESSION['id_User'];
$sql ="SELECT c.Id_comanda,c.c_date,(SELECT username FROM Users WHERE id_User = c.Id_User) AS USER ,
c.status_comanda,(SELECT SUM(`Cantitate`*`Pret_Unitar`) from comenzi_detalii WHERE id_comanda=c.Id_Comanda GROUP BY Id_comanda ) AS Total
 FROM COMENZI c INNER JOIN USERS u ON c.id_User = u.id_User";


?>

<?php
  $id_user=$_SESSION['id_User'];
  ?>
  <br>
<table style="position:relative;width:100%"id="dtBasicExample" class="table table-bordered  table-striped " cellspacing="0" width="100%">
  <thead>
    <tr>
      <th class="th-sm"scope="col">Username</th>
      <th class="th-sm"scope="col">Nr. Comanda</th>
      <th class="th-sm"scope="col">Data</th>
      <th class="th-sm"scope="col">Pret Total</th>
      <th class="th-sm"scope="col">Status</th>
      <th class="th-sm"scope="col">Factura</th>
    </tr>
  </thead>
  <tbody>
  <?php
    $query = mysqli_query($dbconnect, $sql) or die(mysqli_error($dbconnect));

    while($row = mysqli_fetch_array($query)){
    echo "
    <tr>
      <th scope='row'>$row[2]</th>
      <th scope='row'>$row[0]</th>
      <td>$row[1]</td>
      <td>$row[4]</td>
      <td>$row[3]</td>
      <td><a onclick ='openFactura($row[0]);' href='#' >Deschide</a></td>
    </tr>";
    }
    ?>
  </tbody>
</table>
<div id="dialog" style="display:none">
    <iframe id="iframe_pdf" class="iframe_pdf" src=""></iframe>
</div>
<script>
        $(document).ready(function () {
        $('.dataTables_length').addClass('bs-select');
        });
    function openFactura(id){
			$("#iframe_pdf").attr("src","comenzi_admin.php?id_comanda="+id);
			$( "#dialog" ).dialog({
				autoOpen: false, 
				modal: true,
				title: "FACTURA",
				hide: "puff",
				show : "slide",
				position: {
					my: " top ",
					at: " top ",
					collision: "fit",
				 }
			 });
			$("#dialog").dialog("open");
    }
</script>