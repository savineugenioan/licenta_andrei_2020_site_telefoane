<?php

// require MySQL Connection
require ('database/DBController.php');

// require Product Class
require ('database/Product.php');

// require Cart Class
require ('database/Cart.php');
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}


// DBController object
$db = new DBController();
$dbconnect = $db->con;
// Product object
$product = new Product($db);
$product_shuffle = $product->getData();

// Cart object
$Cart = new Cart($db );
